#!/usr/bin/python3
# -*- coding: utf-8 -*-

from smil import SMILHandler
from xml.sax import make_parser
from urllib.request import urlretrieve
import sys
import json

class Karaoke():

    def __init__(self, fichero):
        parser = make_parser()
        cHandler = SMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(fichero))
        self.list = cHandler.get_tags()

    def __str__(self):
        parrafo = ""
        for elem in self.list:
            parrafo = parrafo + elem[0]
            elementos = elem[1].items()
            for nombre, valor in elementos:
                if valor != "":
                    if valor != elementos:
                        parrafo = parrafo + '\t' + nombre + '=' + '"' + valor + '"'
            parrafo += '\n'
        print(parrafo)

    def to_json(self):
        filejson = fichero.replace(".smil", ".json")
        with open(filejson, "w") as jsonfile:
            json.dump(self.list, jsonfile, indent=2)

    def download(self):
        numero = -1
        nombre = ""
        parrafo = ""
        for elem in self.list:
            parrafo = parrafo + elem[0]
            elementos = elem[1].items()
            for nombre, valor in elementos:
                if valor.startswith('http://'):
                    numero += 1
                    print(valor)
                    archivo = valor.split('/')[-1]
                    extension = archivo.split('.')[-1]
                    nombre = "fichero-" +str(numero)+"."+extension
                    urlretrieve(valor, nombre)


if __name__ == "__main__":

    if(sys.argv[1]=='--json'):
        sys.argv.pop(1)
        try:
            obj = open(sys.argv[1])
            fichero = sys.argv[1]
            obj = Karaoke(fichero)
            obj.__init__(fichero)
            print("json")
            obj.to_json()
            obj.download()
        except (ValueError, IndexError, FileNotFoundError):
            sys.exit("Usage: python3 karaoke.py --json file.smil")
    else:
        try:
            obj = open(sys.argv[1])
            fichero = sys.argv[1]
            obj = Karaoke(fichero)
            obj.__init__(fichero)
            obj.__str__()
            obj.download()
        except (ValueError, IndexError, FileNotFoundError):
            sys.exit("Usage: python3 karaoke.py file.smil")
