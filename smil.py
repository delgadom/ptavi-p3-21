#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler

class SMILHandler(ContentHandler):
    def __init__(self):
        self.list = []
        self.arra = {'root-layout': ['width', 'height', 'background-color'],
                     'region': ['id', 'top', 'bottom', 'left', 'right'],
                     'img': ['src', 'region', 'begin','end', 'dur'],
                     'audio': ['src', 'begin', 'dur'],
                     'textstream': ['src', 'region', 'fill']}

    def startElement(self, name, attrs):
        dict = {}
        if name in self.arra:
            for atrib in self.arra[name]:
                dict[atrib] = attrs.get(atrib, "")
            self.list.append([name, dict])

    def get_tags(self):
        return self.list

if __name__ == "__main__":
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
